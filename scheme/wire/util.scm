(define-module (wire util))

(use-modules (geda attrib))
(use-modules (geda object))
(use-modules (gschem selection))
(use-modules (gschem window))
(use-modules (gschem attrib))
(use-modules (geda page))

;return #t if attribute name is the same as name
(define (attrib-name? attrib name)
  (and (attribute? attrib)
       (string=? (attrib-name attrib) name)))

;return #t if it is a net attribute, otherwise #f
(define (attribute-net? attrib)
  (attrib-name? attrib "net"))

;return #t if attrib is a value attribute, otherwise #f
(define (attribute-value? attrib)
  (attrib-name? attrib "value"))

;return #t if component have only one pin and net attribute, otherwise #f
(define (component-special? component)
  (and (component? component)
       (= (length (filter pin? (component-contents component))) 1) 
       (or (= (length (filter attribute-net? (object-attribs component))) 1) 
	   (and (= (length (filter attribute-net? (inherited-attribs component))) 1)
		(= (length (filter attribute-net? (object-attribs component))) 0)))))

;return #t if component is a component with only one pin, net attribute and value attribute, otherwise return #f
(define (component-tag? component)
  (and (component-special? component)
       (not (null? (filter attribute-value? (object-attribs component))))))

;return #t if component is a component with only one pin and net attribute but no value attribute, otherwise return #f
(define (component-power? component)
  (and (component-special? component)
       (null? (filter attribute-value? (object-attribs component)))
       (null? (filter attribute-value? (inherited-attribs component)))))

;return value of first occurence of net attribute in component
;note: component must contains at least one net attribute otherwise it will throw an error
(define (first-net-attrib-value component)
  (attrib-value (list-ref (filter attribute-net? (append (object-attribs component) (inherited-attribs component))) 0)))

;return #t if at least one element from list1 is in list2, otherwise #f
(define (contains>=1? list1 list2)
  (let loop1 ((x (1- (length list1))))
    (if (negative? x)
	#f
	(if (not (member (list-ref list1 x) list2))
	    (loop1 (1- x))
	    #t))))

;return #t if at least one element from list1 is missing from list2. otherwise #f
(define (miss>=1? list1 list2)
  (let loop1 ((x (1- (length list1))))
    (if (negative? x)
	#f
	(if (not (member (list-ref list1 x) list2))
	    #t
	    (loop1 (1- x))))))

;join selected wire tags together
(define (tag-join)
  (define highest 0)
  (define num 0)
  (for-each (lambda (page) ;for each page
	      (for-each (lambda (attr) ;for each net attribute in page
			  (set! num (string->number (substring (attrib-value attr) 0 (string-index (attrib-value attr) #\:)))) ;get signalname part of net attribute value (signalname:pinanme,...)
			  (when (and (exact-integer? num) (> num highest)) ;if num is a valid number and it is higher than the highest value
			    (set! highest num))) ;save highest value
			(filter attribute-net? (page-contents page))))
	    (active-pages))
  (set! highest (1+ highest)) ;set next tag value
  (for-each (lambda (tag) ;for each component tag selected in current page
	      (if (null? (filter attribute-net? (object-attribs tag))) ;if net attribute is not overriden
		  (add-attrib! tag "net" (string-append (number->string highest) ":1") #f 'both) ;add net attribute to override internal one
		  (set-attrib-value! (list-ref (filter attribute-net? (object-attribs tag)) 0) (string-append (number->string highest) ":1"))) ;change value of the promoted one
	      (set-attrib-value! (list-ref (filter attribute-value? (object-attribs tag)) 0) (number->string highest))) ;change value attribute
	    (filter component-tag? (page-selection (active-page))))
  #t) ;return #t when procedure is done

;cut selected net and place wire tag at each end
;tag: component basename to use as tag
(define (wire-cut tag)
  (define net (list-ref (filter net? (page-selection (active-page))) 0))
  (define x1 (car (line-start net)))
  (define x2 (car (line-end net)))
  (define y1 (cdr (line-start net)))
  (define y2 (cdr (line-end net)))
  (define components #f)
  (for-each (lambda (attr) ;detach and remove all attached attributes
	      (detach-attribs! net attr)
	      (page-remove! (active-page) attr))
	    (object-attribs net))
  (page-remove! (active-page) net) ;remove net
  (cond
    ((= x1 x2) ;vertical line
     (set! components (cons ;save created components for further use
			(make-component/library tag (cons (- x1 600) (+ (max y1 y2) 1300)) 270 #f #f)
			(make-component/library tag (cons (- x1 600) (- (min y1 y2) 1300)) 270 #t #f)))) ;add wire tags and translate them so that they connect to old net connections
    ((= y1 y2) ;horizontal line
     (set! components (cons ;save created components for further use
			(make-component/library tag (cons (- (min x1 x2) 1300) (- y1 600)) 0 #f #f)
			(make-component/library tag (cons (+ (max x1 x2) 1300) (- y1 600)) 0 #t #f))))) ;add wire tags and translate them so that they connect to old net connections
  (set-component-with-transform! (car components) (component-position (car components)) (component-angle (car components)) (component-mirror? (car components)) (component-locked? (car components)))
  (set-component-with-transform! (cdr components) (component-position (cdr components)) (component-angle (cdr components)) (component-mirror? (cdr components)) (component-locked? (cdr components)))
  (page-append! (active-page) (car components) (cdr components)) ;add tags to the page (display them)
  (promote-attribs! (car components))
  (promote-attribs! (cdr components)) ;promote all promotable attributes of tags
  (select-object! (car components))
  (select-object! (cdr components)) ;select created tags
  (tag-join)) ;join selected tags together

(export attrib-name? attribute-net? attribute-value? component-special? component-tag? component-power? first-net-attrib-value contains>=1? miss>=1? tag-join wire-cut)
