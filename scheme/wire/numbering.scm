(define-module (wire numbering))

(use-modules (gschem window))
(use-modules (gschem attrib))
(use-modules (geda page))
(use-modules (geda object))
(use-modules (geda attrib))
(use-modules (srfi srfi-1))
(use-modules (wire util))

#!
;return #t if element is in a list of list (lol), otherwise #f
(define (contains? element lol)
  ;loop from last to first element in lists until same object is found
  (let loop1 ((x (1- (length lol)))) ;iterate through first level list
    (if (negative? x) ;if searched all list
	#f ;no match found, stop loop1
	(or (let loop2 ((y (1- (length (list-ref lol x))))) ;if not end of first level list, iterate through second level list
	  (if (negative? y) ;if searched all list
	      #f ;no match found, stop loop2
	      (if (eq? (list-ref (list-ref lol x) y) element) ;if not end of second level list, check if elements match
		  #t ;we found a match, stop loop2
		  (loop2 (1- y))))) ;no match found, continue loop2
	    (loop1 (1- x)))))) ;no match found in second level list, continue to next second level list

;return #t if endpoint is connected to net, otherwise #f
(define (connected-endpoint? net endpoint)
  (define x1 0)
  (define x2 0)
  (define y1 0)
  (define y2 0)
  (define a 0) ;slope
  (define b 0) ;y intercept
  (if (and (net? net) (pair? endpoint)) ;check if parameters are valid types
      (begin
	(set! x1 (car (line-start net)))
	(set! x2 (car (line-end net)))
	(set! y1 (cdr (line-start net)))
	(set! y2 (cdr (line-end net)))
	(cond ((and (= (car endpoint) x1 x2)
		    (>= (cdr endpoint) (min y1 y2))
		    (<= (cdr endpoint) (max y1 y2))) ;vertical line and endpoint is on that line
	       #t)
	      ((and (= (cdr endpoint) y1 y2)
		    (>= (car endpoint) (min x1 x2))
		    (<= (car endpoint) (max x1 x2))) ;horizontal line and endpoint is on that line
	       #t)
	      ((and (>= (car endpoint) (min x1 x2))
		    (<= (car endpoint) (max x1 x2))
		    (>= (cdr endpoint) (min y1 y2))
		    (<= (cdr endpoint) (max y1 y2))) ;slope line and endpoint is in line domain (between endpoints of net line)
	       (set! a (/ (- y2 y1) (- x2 x1))) ;calculate line slope
	       (set! b (- y1 (* a x1))) ;calculate y intercept
	       (if (= (+ (* a (car endpoint)) b) (cdr endpoint)) ;if endpoint fits in line equation (y=ax+b)
		  #t
		  #f))
	      (else #f))) ;no endpoint connection to net
      #f))

;return #t if the two nets are connected, otherwise #f
(define (connected-net? net1 net2)
  (if (and (net? net1) (net? net2)) ;ensure nets are valid nets
      (if (or
	    (connected-endpoint? net1 (line-start net2))
	    (connected-endpoint? net1 (line-end net2))
	    (connected-endpoint? net2 (line-start net1))
	    (connected-endpoint? net2 (line-end net1))) ;if nets are connected somehow
	  #t
	  #f)
      #f))
!#
#!
;return #t if attribute name is the same as name
(define (attrib-name? attrib name)
  (and (attribute? attrib)
       (string=? (attrib-name attrib) name)))

;return #t if it is a net attribute, otherwise #f
(define (attribute-net? attrib)
  (attrib-name? attrib "net"))

;return #t if attrib is a value attribute, otherwise #f
(define (attribute-value? attrib)
  (attrib-name? attrib "value"))

;return #t if component have only one pin and net attribute, otherwise #f
(define (component-special? component)
  (and (component? component)
       (= (length (filter pin? (component-contents component))) 1) 
       (or (= (length (filter attribute-net? (object-attribs component))) 1) 
	   (and (= (length (filter attribute-net? (inherited-attribs component))) 1)
		(= (length (filter attribute-net? (object-attribs component))) 0)))))

;return #t if component is a component with only one pin, net attribute and value attribute, otherwise return #f
(define (component-tag? component)
  (and (component-special? component)
       (not (null? (filter attribute-value? (object-attribs component))))))

;return #t if component is a component with only one pin and net attribute but no value attribute, otherwise return #f
(define (component-power? component)
  (and (component-special? component)
       (null? (filter attribute-value? (object-attribs component)))
       (null? (filter attribute-value? (inherited-attribs component)))))

;return value of first occurence of net attribute in component
;note: component must contains at least one net attribute otherwise it will throw an error
(define (first-net-attrib-value component)
  (attrib-value (list-ref (filter attribute-net? (append (object-attribs component) (inherited-attribs component))) 0)))

;return #t if at least one element from list1 is in list2, otherwise #f
(define (contains>=1? list1 list2)
  (let loop1 ((x (1- (length list1))))
    (if (negative? x)
	#f
	(if (not (member (list-ref list1 x) list2))
	    (loop1 (1- x))
	    #t))))

;return #t if at least one element from list1 is missing from list2. otherwise #f
(define (miss>=1? list1 list2)
  (let loop1 ((x (1- (length list1))))
    (if (negative? x)
	#f
	(if (not (member (list-ref list1 x) list2))
	    #t
	    (loop1 (1- x))))))
!#
;find connected nets and pins in pages
;pages: list of pages to search for connected nets and pins
(define (find-connected pages)
  (define connected '()) ;empty connected nets and pins list
  (define objects '()) ;empty objects list
  (define connected-objects '()) ;empty list of directly connected objects including single pin components with same net attribute
  (define already-connected #f)
  (define delete-connected '())
  (define new-connected '())
  ;create a list with the content of all pages
  (for-each (lambda (page) ;for each page in pages
	      (set! objects (append objects (filter net? (page-contents page)) (filter component? (page-contents page))))) ;add nets and components to the objects to search for connections
	    pages)
  (for-each (lambda (object) ;for each object
	      (set! connected-objects '()) ;reset connected-objects list
	      (if (component-special? object) ;if object is a single pin and net attribute component
		(begin (set! connected-objects (append (filter pin? (component-contents object)) (object-connections object))) ;add component pin and object connections to list of directly connected objects
		       (for-each (lambda (obj) ;for each other objects
				   (when (and (component-special? obj) (string=? (first-net-attrib-value object) (first-net-attrib-value obj))) ;if object is a single pin and net component and the two net attribute have the same value
				     (set! connected-objects (append connected-objects (filter pin? (component-contents obj)))))) ;add component pin to list of directly connected objects
				 (delete object objects))
		       (set! connected-objects (list connected-objects))) ;put connected-objects list in a list
		(if (component? object) ;if object is not a special component but a regular one
		    (for-each (lambda (pin) ;put each pins with object-connections in a separate list in connected-objects
				(when (> (length (object-connections pin)) 0) ;if pin is connected
				  (set! connected-objects (append connected-objects (list (append (list pin) (object-connections pin)))))))
			      (filter pin? (component-contents object)))
		    (set! connected-objects (list (append (list object) (object-connections object)))))) ;just a net, put all its connections in a list including itself
	      (for-each (lambda (connected-objects-list)
				      (when already-connected (set! already-connected #f)) ;reset already-connected to #f
				      (when (not (equal? delete-connected connected)) (set! delete-connected connected)) ;reset delete-connected to connected
				      (when (not (null? new-connected )) (set! new-connected '())) ;reset new-connected to '()
				      (for-each (lambda (connected-list) ;for each list of already found connected objects
						  (when (contains>=1? connected-objects-list connected-list) ;if connected-list contains at least one object from connected-objects-list
						    (when (not already-connected) (set! already-connected #t)) ;dont need to add a new list in connected list
						    (when (miss>=1? connected-objects-list connected-list) ;if at least one object from connected-objects-list is missing from connected-list
						      (set! new-connected (append new-connected connected-list connected-objects-list))
						      (set! delete-connected (delete connected-list delete-connected)))))
						  connected)
				      (when (not already-connected)
					(set! connected (append connected (list connected-objects-list)))) ;no element of connected-objects-list in connected, so add them in a new list
				      (when (not (null? new-connected))
					(set! connected (append delete-connected (list (delete-duplicates new-connected)))))) ;if we found new connections, save modifications to connected
			  connected-objects))
	    objects)
  connected) ;return connected nets and pins list

;return net line length
(define (net-length net)
  (define x1 (car (line-start net)))
  (define x2 (car (line-end net)))
  (define y1 (cdr (line-start net)))
  (define y2 (cdr (line-end net)))
  (sqrt (+ (expt (- x2 x1) 2) (expt (- y2 y1) 2)))) ;a^2 + b^2 = c^2

;return the longest net from a net list
(define (longest netlist)
  (define long (make-net '(0 . 0) '(0 . 0))) ;0 length net
  (for-each (lambda (net)
	      (when (>= (net-length net) (net-length long))
		(set! long net))) ;when a longer net is found keep it
	    netlist)
  long) ;return longest net found

;add wirenum attribute in the middle of a net
;net: net to add wirenum attribute
;value: wire number attribute value
(define (add-wirenum-net net value)
  (define x1 (car (line-start net)))
  (define x2 (car (line-end net)))
  (define y1 (cdr (line-start net)))
  (define y2 (cdr (line-end net)))
  (define attr #f)
  (set! attr (cond ;save created attribute
    ((= x1 x2) ;vertical net
     (make-text (cons (- x1 100) (+ (min y1 y2) (* (quotient (/ (net-length net) 2) 100) 100))) 'middle-right 0 (string-append "wirenum=" (number->string value)) 10 #t 'value))
    ((= y1 y2) ;horizontal net
     (make-text (cons (+ (min x1 x2) (* (quotient (/ (net-length net) 2) 100) 100)) (+ y1 100)) 'lower-center 0 (string-append "wirenum=" (number->string value)) 10 #t 'value))
    (else #f))) ;diagonal net not supported
  (unless (eq? attr #f) ;unless net is not supported, do...
    (page-append! (object-page net) attr) ;add attribute to page
    (attach-attribs! net attr))) ;attach attribute to net

;add wirenum attribute to each component-tag in lst
;lst: list of connected nets and pins
;num: wire number attribute value
(define (add-wirenum-tag lst num)
  (define pos #f) ;position (x . y)
  (define attr #f) ;attribute object
  (for-each (lambda (tag) ;for each component-tag in lst
              (set! pos (component-position tag)) ;retrieve component position
              (set-cdr! pos (+ (cdr pos) 100)) ;set wirenum attribute position
              (set! attr (make-text pos 'lower-left 0 (string-append "wirenum=" (number->string num)) 10 #f 'both)) ;create wirenum attribute
              (page-append! (object-page tag) attr) ;add attribute to page
              (attach-attribs! tag attr) ;attach attribute to wire tag
	      (set-attrib-value! (list-ref (filter attribute-value? (object-attribs tag)) 0) (number->string num)) ;change value attribute
	      (if (null? (filter attribute-net? (object-attribs tag))) ;if net attribute is not overriden
                  (begin
		    (set! pos (text-anchor (list-ref (filter attribute-net? (inherited-attribs tag)) 0))) ;retrieve inherited net attribute position
                    (set! attr (make-text pos 'lower-left 0 (string-append "net=" (number->string num) ":1") 10 #f 'both)) ;create net attribute
                    (page-append! (object-page tag) attr) ;add attribute to page
                    (attach-attribs! tag attr)) ;attach attribute to wire tag
		  (set-attrib-value! (list-ref (filter attribute-net? (object-attribs tag)) 0) (string-append (number->string num) ":1")))) ;change value of the promoted one
	    (filter component-tag? (map (lambda (pin) (object-component pin)) (filter pin? lst)))))

;add wirenum attribute to pin connection point
;pin: pin to attach wirenum
;num: wire number attribute value
(define (add-wirenum-pin pin num)
  (define x1 (car (line-start pin)))
  (define x2 (car (line-end pin)))
  (define y1 (cdr (line-start pin)))
  (define y2 (cdr (line-end pin)))
  (define attr #f) ;attribute object
  (define pos #f) ;position (x . y)
  (set! attr (cond
    ((= x1 x2) ;vertical pin   
     (set! pos (cons (- (car (line-start pin)) 100) (cdr (line-start pin))))
     (make-text pos 'middle-right 0 (string-append "wirenum=" (number->string num)) 10 #t 'value))
    ((= y1 y2) ;horizontal net
     (set! pos (cons (car (line-start pin)) (+ (cdr (line-start pin)) 100)))
     (make-text pos 'lower-center 0 (string-append "wirenum=" (number->string num)) 10 #t 'value))
    (else #f))) ;diagonal pins not supported
  (unless (eq? attr #f) ;unless pin is not supported, do...
    (page-append! (object-page pin) attr) ;add attribute to page
    (attach-attribs! (object-component pin) attr))) ;attach attribute to component containing pin

;return #t if a wirenum attrib is attached to object, otherwise #f
(define (wirenum-attached? object)
  (define attribs (object-attribs object)) ;get list of attributes attached to object
  (if (> (length attribs) 0) ;if objet contains attributes
      (let wirenum? ((x (1- (length attribs)))) ;iterate through attribute list
	(if (string=? (attrib-name (list-ref attribs x)) "wirenum") ;if we found wirenum attribute
	    #t ;stop searching
	    (if (<= x 0) ;reached end of list and wirenum attrib not found
		#f ;stop searching
		(wirenum? (1- x))))) ;continue searching
      #f))

;return the value of the highest wirenum attribute value in pages. if no wirenum attribute are found, return 0
(define (highest-wirenum pages)
  (define highest 0)
  (for-each (lambda (page) ;for each page
	 (for-each (lambda (attr) ;for each attribute in page
		(when (and (string=? (attrib-name attr) "wirenum") (> (string->number (attrib-value attr)) highest)) ;if attribute name is wirenum and attribute value is greater than the highest one
		  (set! highest (string->number (attrib-value attr))))) ;save highest wirenum value
	      (filter attribute? (page-contents page))))
       pages)
  highest)

;place wirenum attribute
;return #t if at least one wirenum has been placed, otherwise #f
;lst: list of connected objects
;num: wirenum attribute value
(define (place-wirenum lst num)
  (if (null? (filter component-power? (map (lambda (pin) (object-component pin)) (filter pin? lst)))) ;dont add wirenum to power wires, info is supplied by text in the symbol itself
    (begin
      (if (not (null? (filter net? lst))) ;if at least one net is present in list
	(begin
	  (add-wirenum-net (longest (filter net? lst)) num) ;add wirenum attribute to the longest net
	  (add-wirenum-tag lst num)) ;add wirenum to each component-tag
	(if (not (null? (filter component-tag? (map (lambda (pin) (object-component pin)) (filter pin? lst))))) ;if no nets but at least one component-tag
	    (add-wirenum-tag lst num) ;add wirenum to each component-tag
	    (add-wirenum-pin (list-ref lst 0) num))) ;if no nets and no component-special, add wirenum in the middle of the connection point of pins
      #t) ;wirenum placed
    #f)) ;no wirenum placed

;automatic wire numbering
;overwrite: overwrite existing numbers if true, otherwise just add the missing ones
;pages: list of page to apply wire numbering
;start: if supplied, starting number for wirenum attribute otherwise, start from the highest wirenum attribute value + 1 found in pages
(define (wirenum overwrite pages . start)
  (define connected '()) ;empty connected nets and pins list
  (define num #f) ;wirenum attribute value
  (define attribs '()) ;empty attribute list
  (set! connected (find-connected pages)) ;find connected nets and pins in pages
  (if (> (length start) 0) ;if starting wirenum value is supplied
      (set! num (list-ref start 0)) ;save starting number
      (set! num (1+ (highest-wirenum pages)))) ;use the highest wirenum value + 1 as starting number
  (for-each (lambda (connected-list)
         (define object #f)
	 (if (let wirenum? ((x (1- (length connected-list)))) ;if connected-list contains an object with attached wirenum attribute
	     (if (< x 0) ;reached end of list
	       #f ;stop searching
	       (begin
		 (set! object (list-ref connected-list x))
	         (if (or (wirenum-attached? object) (and (pin? object) (wirenum-attached? (object-component object))))  ;if a wirenum attribute is attached to object or to component containing object
	           #t ;stop searching
	           (wirenum? (1- x)))))) ;continue searching
	     (when overwrite ;when we need to overwrite existing attributes
	       ;delete all wirenum attribute in connected-list
	       (for-each (lambda (object) ;for each object in connected-list
			   (set! attribs (object-attribs object))
			   (when (pin? object) ;if object is a pin
			     (set! attribs (append attribs (object-attribs (object-component object))))) ;add component attributes to list
		           (for-each (lambda (attr) ;for each attribute attached to object and component containing object
				       (when (string=? (attrib-name attr) "wirenum") ;when it is wirenum attribute
					 (detach-attribs! (attrib-attachment attr) attr) ;detach attribute
			                 (page-remove! (object-page attr) attr))) ;remove attribute
			               attribs))
		           connected-list)
	       (when (place-wirenum connected-list num)
		 (set! num (1+ num)))) ;increment wirenum number on successfull placement only
	     (begin ;if connected-list does not contains an object with a wirenum attribute attached
	       (when (place-wirenum connected-list num)
	         (set! num (1+ num)))))) ;increment wirenum number only when a number has been placed
	 connected)
  #t)

;export symbols to the public interface
(export wirenum)
