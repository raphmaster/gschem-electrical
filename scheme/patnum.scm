(use-modules (ice-9 regex))
(use-modules (geda page))
(use-modules (geda object))
(use-modules (geda attrib))

;replace question mark '?' or integer number with incremented number in all attributes in pages matching regular expression pattern
;pattern: extended regular expression must include attribute name followed by the equal sign '=' (ex: refdes=U\\? matches all refdes=U? attributes)
;opts: list of optional arguments in the following order : (pages start)
;pages: (optional) list of pages to apply pattern numbering (default: all active pages)
;start: (optional) starting number from which we increment after each successfull match (default: 1)
(define (patnum pattern . opts)
    (define pages '())
    (define start 1)
    (define r1 (make-regexp "="))
    (define r2 #f)
    (define r3 (make-regexp "\\?|[0-9]+"))
    (define newstr "")
    (cond
	((= (length opts) 0) ;no optional arguments set default values
	    (set! pages (active-pages))
	    (set! start 1)
	)
	((= (length opts) 1) ;one optional arguments try to guess right one
	    (cond
		((and (list? (list-ref opts 0)) (not (null? (list-ref opts 0))) (page? (list-ref (list-ref opts 0) 0))) ;if argument is a list, it is non-empty and first element is a page
		    (set! pages (list-ref opts 0)) ;argument guessed as list of pages
		)
		((exact-integer? (list-ref opts 0))
		    (set! start (list-ref opts 0))
		)
	    )
	)
	((= (length opts) 2) ;first argument must be list of pages and second exact integer
	    (set! pages (list-ref opts 0))
	    (set! start (list-ref opts 1))
	)
    )
    (when (not (eq? (regexp-exec r1 pattern) #f)) ;if pattern contains '='
	(set! r2 (make-regexp pattern)) ;compile regular expression
	(for-each ;for each page
	    (lambda (p)
		(for-each ;for each object in page
		    (lambda (o)
			(when (and (attribute? o) (not (eq? (regexp-exec r2 (text-string o)) #f))) ;if object is an attribute and matches pattern
			    (set! newstr "") ;reset new attribute text
			    (cond
				((not (eq? (regexp-exec r3 (text-string o)) #f)) ;if attribute text contains '?' or number
				    (set! newstr (regexp-substitute #f (regexp-exec r3 (text-string o)) 'pre (number->string start))) ;save new attribute text
				)
			    )
			    (when (not (string-null? newstr)) ;if pattern numbering was successfull
				(set-attrib-value! o (regexp-substitute #f (regexp-exec r1 newstr) 'post)) ;set new attribute value
				(set! start (+ start 1)) ;increment number
			    )
			)
		    )
		(page-contents p)
		)
	    )
	pages
	)
    )
)
