(use-modules (geda object))
(use-modules (geda page))
(use-modules (gschem window))
(use-modules (gschem action))
(use-modules (gschem attrib))
(use-modules (gschem selection))

;add standard symbol atributes
(define-action (&add-std-sym-attr
		 #:name "Add Standard Symbol Attributes")
  (page-append! (active-page)
    (make-text '(0 . 0) 'lower-left 0 "device=none" 10 #f 'both 8)
    (make-text '(0 . 200) 'lower-left 0 "description=none" 10 #f 'both 8)
    (make-text '(0 . 400) 'lower-left 0 "author=Raphael Gosselin raphgoss69ATgmail.com" 10 #f 'both 8)
    (make-text '(0 . 600) 'lower-left 0 "numslots=0" 10 #f 'both 8)
    (make-text '(0 . 800) 'lower-left 0 "footprint=none" 10 #f 'both 8)
    (make-text '(0 . 1000) 'lower-left 0 "documentation=none" 10 #f 'both 8)
    (make-text '(0 . 1200) 'lower-left 0 "comment=none" 10 #f 'both 8)
    (make-text '(0 . 1400) 'lower-left 0 "symversion=1.0" 10 #f 'both 8)
    (make-text '(0 . 1600) 'lower-left 0 "dist-license=GPLv3" 10 #f 'both 8)
    (make-text '(0 . 1800) 'lower-left 0 "use-license=unlimited" 10 #f 'both 8)
    (make-text '(0 . 2000) 'lower-left 0 "value=value" 10 #t 'value 8)
    (make-text '(0 . 2200) 'lower-left 0 "refdes=refdes" 10 #t 'value 8)))

;add keys binding
(global-set-key "A S" &add-std-sym-attr)

;add standard pin attributes
(define-action (&add-std-pin-attr
		 #:name "Add Standard Pin Attributes")
	       (define pos (pointer-position)) ;get pointer position
	       (when (not pos) ;when cursor is outside display area
		 (set! pos '(0 . 600))) ;display attributes at the origin
	       (set! pos (snap-point pos)) ;get nearest snap point from pos
	       (for-each (lambda (attr) ;for each created attributes
			   (page-append! (active-page) attr) ;add attribute to page
			   (when (= (length (filter pin? (page-selection (active-page)))) 1) ;if only one pin is selected
			     (attach-attribs! (list-ref (filter pin? (page-selection (active-page))) 0) attr))) ;attach attribute to pin
			 (list
			   (make-text (cons (car pos) (cdr pos)) 'lower-left 0 "pinseq=0" 10 #f 'both 8) ;add pin attributes
			   (make-text (cons (car pos) (- (cdr pos) 200)) 'lower-left 0 "pintype=unknown" 10 #f 'both 8)
			   (make-text (cons (car pos) (- (cdr pos) 400)) 'lower-left 0 "pinnumber=0" 10 #t 'value 8)
			   (make-text (cons (car pos) (- (cdr pos) 600)) 'lower-left 0 "pinlabel=unknown" 10 #t 'value 8))))

;add shortcut for pin attributes
(global-set-key "A D" &add-std-pin-attr)
